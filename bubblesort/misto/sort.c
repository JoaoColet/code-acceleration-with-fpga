#include <stdio.h>
#include "xparameters.h"
#include "core_sort.h"
#include "xtmrctr.h"
#define SIZE 16

int teste_sort_vhdl(int *vin, int *vout){
	int i;
	for(i = 0; i<7; i++){
		CORE_SORT_mWriteReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG0_OFFSET, vin[0]);
		CORE_SORT_mWriteReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG1_OFFSET, vin[1]);
		CORE_SORT_mWriteReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG2_OFFSET, vin[2]);
		CORE_SORT_mWriteReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG3_OFFSET, vin[3]);
		CORE_SORT_mWriteReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG4_OFFSET, vin[4]);
		CORE_SORT_mWriteReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG5_OFFSET, vin[5]);
		CORE_SORT_mWriteReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG6_OFFSET, vin[6]);
		CORE_SORT_mWriteReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG7_OFFSET, vin[7]);
		CORE_SORT_mWriteReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG8_OFFSET, vin[8]);
		CORE_SORT_mWriteReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG9_OFFSET, vin[9]);
		CORE_SORT_mWriteReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG10_OFFSET, vin[10]);
		CORE_SORT_mWriteReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG11_OFFSET, vin[11]);
		CORE_SORT_mWriteReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG12_OFFSET, vin[12]);
		CORE_SORT_mWriteReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG13_OFFSET, vin[13]);
		CORE_SORT_mWriteReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG14_OFFSET, vin[14]);
		CORE_SORT_mWriteReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG15_OFFSET, vin[15]);

		vin[0] = CORE_SORT_mReadReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG16_OFFSET);
		vin[1] = CORE_SORT_mReadReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG17_OFFSET);
		vin[2] = CORE_SORT_mReadReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG18_OFFSET);
		vin[3] = CORE_SORT_mReadReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG19_OFFSET);
		vin[4] = CORE_SORT_mReadReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG20_OFFSET);
		vin[5] = CORE_SORT_mReadReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG21_OFFSET);
		vin[6] = CORE_SORT_mReadReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG22_OFFSET);
		vin[7] = CORE_SORT_mReadReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG23_OFFSET);
		vin[8] = CORE_SORT_mReadReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG24_OFFSET);
		vin[9] = CORE_SORT_mReadReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG25_OFFSET);
		vin[10] = CORE_SORT_mReadReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG26_OFFSET);
		vin[11] = CORE_SORT_mReadReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG27_OFFSET);
		vin[12] = CORE_SORT_mReadReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG28_OFFSET);
		vin[13] = CORE_SORT_mReadReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG29_OFFSET);
		vin[14] = CORE_SORT_mReadReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG30_OFFSET);
		vin[15] = CORE_SORT_mReadReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG31_OFFSET);
	}
	CORE_SORT_mWriteReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG0_OFFSET, vin[0]);
	CORE_SORT_mWriteReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG1_OFFSET, vin[1]);
	CORE_SORT_mWriteReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG2_OFFSET, vin[2]);
	CORE_SORT_mWriteReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG3_OFFSET, vin[3]);
	CORE_SORT_mWriteReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG4_OFFSET, vin[4]);
	CORE_SORT_mWriteReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG5_OFFSET, vin[5]);
	CORE_SORT_mWriteReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG6_OFFSET, vin[6]);
	CORE_SORT_mWriteReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG7_OFFSET, vin[7]);
	CORE_SORT_mWriteReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG8_OFFSET, vin[8]);
	CORE_SORT_mWriteReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG9_OFFSET, vin[9]);
	CORE_SORT_mWriteReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG10_OFFSET, vin[10]);
	CORE_SORT_mWriteReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG11_OFFSET, vin[11]);
	CORE_SORT_mWriteReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG12_OFFSET, vin[12]);
	CORE_SORT_mWriteReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG13_OFFSET, vin[13]);
	CORE_SORT_mWriteReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG14_OFFSET, vin[14]);
	CORE_SORT_mWriteReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG15_OFFSET, vin[15]);

	vout[0] = CORE_SORT_mReadReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG16_OFFSET);
	vout[1] = CORE_SORT_mReadReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG17_OFFSET);
	vout[2] = CORE_SORT_mReadReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG18_OFFSET);
	vout[3] = CORE_SORT_mReadReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG19_OFFSET);
	vout[4] = CORE_SORT_mReadReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG20_OFFSET);
	vout[5] = CORE_SORT_mReadReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG21_OFFSET);
	vout[6] = CORE_SORT_mReadReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG22_OFFSET);
	vout[7] = CORE_SORT_mReadReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG23_OFFSET);
	vout[8] = CORE_SORT_mReadReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG24_OFFSET);
	vout[9] = CORE_SORT_mReadReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG25_OFFSET);
	vout[10] = CORE_SORT_mReadReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG26_OFFSET);
	vout[11] = CORE_SORT_mReadReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG27_OFFSET);
	vout[12] = CORE_SORT_mReadReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG28_OFFSET);
	vout[13] = CORE_SORT_mReadReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG29_OFFSET);
	vout[14] = CORE_SORT_mReadReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG30_OFFSET);
	vout[15] = CORE_SORT_mReadReg(XPAR_CORE_SORT_0_BASEADDR, CORE_SORT_SLV_REG31_OFFSET);
	return 1;
}

int teste_sort_c(int *vet_c){
	int aux, i, j;
    for (i=0; i<SIZE-1; i++){
    	for(j=i+1; j<SIZE; j++){
    		if(vet_c[i] > vet_c[j]){
    			aux = vet_c[i];
    			vet_c[i] = vet_c[j];
    			vet_c[j] = aux;
    		}
    	}
    }
    return 1;
}

int main(){
	XTmrCtr xps_timer_0;
	XTmrCtr* timer_0 = &xps_timer_0;
	Xuint32 BeginTime;
	Xuint32 EndTime;
	Xuint32 Calibration;
	Xuint32 TimeRun;
	int status;
	status = XTmrCtr_Initialize(&xps_timer_0, XPAR_XPS_TIMER_0_DEVICE_ID);
	XTmrCtr_Start(&xps_timer_0, XPAR_XPS_TIMER_0_DEVICE_ID);

	BeginTime = XTmrCtr_GetValue(timer_0, XPAR_XPS_TIMER_0_DEVICE_ID);
	EndTime = XTmrCtr_GetValue(timer_0, XPAR_XPS_TIMER_0_DEVICE_ID);
	Calibration = EndTime - BeginTime;

	int i, vet_c[SIZE], vet_vhdl[SIZE], vet_vhdl2[SIZE], sortvhdl, bubblesort;

	for(i=0; i<SIZE ; i++){
		vet_c[i]=SIZE-i;
		vet_vhdl[i] = SIZE-i;
		vet_vhdl2[i] = -1;
	}

	xil_printf("\r\nVetor de entrada em ordem decrescente\r\n");

	BeginTime = XTmrCtr_GetValue(&xps_timer_0, XPAR_XPS_TIMER_0_DEVICE_ID);

	sortvhdl = teste_sort_vhdl(vet_vhdl, vet_vhdl2);

	EndTime = XTmrCtr_GetValue(&xps_timer_0, XPAR_XPS_TIMER_0_DEVICE_ID);

	for(i=0; i<SIZE; i++){
		xil_printf("vet_vhdl[%d] = %d\r\n", i, vet_vhdl2[i]);
	}

	//xil_printf("Inicio: %d\r\n", BeginTime); //clock no inicio
	//xil_printf("Fim: %d\r\n", EndTime);		//clock no final

	TimeRun = (EndTime - BeginTime - Calibration);
	xil_printf("Tempo para exec. na FPGA: %d us\r\n", TimeRun/50); //base de tempo 1 clock = 20ns, ou seja 50 clks = 1 us

	BeginTime = XTmrCtr_GetValue(&xps_timer_0, XPAR_XPS_TIMER_0_DEVICE_ID);

	bubblesort = teste_sort_c(vet_c);

	EndTime = XTmrCtr_GetValue(&xps_timer_0, XPAR_XPS_TIMER_0_DEVICE_ID);

	for(i=0; i<SIZE; i++){
		xil_printf("vet_c[%d] = %d\r\n", i, vet_c[i]);
	}

	//xil_printf("Inicio: %d\r\n", BeginTime); //clock no inicio
	//xil_printf("Fim: %d\r\n", EndTime);		//clock no final

	TimeRun = (EndTime - BeginTime - Calibration);
	xil_printf("Tempo para exec. no uC: %d us\r\n\n", TimeRun/50); //base de tempo 1 clock = 20ns

	//---------------------------------------------------------------------//
	for(i=0; i<SIZE ; i++){
		vet_c[i]=i+1;
		vet_vhdl[i] = i+1;
		vet_vhdl2[i] = -1;
	}
	xil_printf("\r\nVetor de entrada em ordem crescente\r\n");

	BeginTime = XTmrCtr_GetValue(&xps_timer_0, XPAR_XPS_TIMER_0_DEVICE_ID);

	sortvhdl = teste_sort_vhdl(vet_vhdl, vet_vhdl2);

	EndTime = XTmrCtr_GetValue(&xps_timer_0, XPAR_XPS_TIMER_0_DEVICE_ID);

	for(i=0; i<SIZE; i++){
		xil_printf("vet_vhdl[%d] = %d\r\n", i, vet_vhdl2[i]);
	}

	//xil_printf("Inicio: %d\r\n", BeginTime); //clock no inicio
	//xil_printf("Fim: %d\r\n", EndTime);		//clock no final

	TimeRun = (EndTime - BeginTime - Calibration);
	xil_printf("Tempo para exec. na FPGA: %d us\r\n", TimeRun/50); //base de tempo 1 clock = 20ns, ou seja 50 clks = 1 us

	BeginTime = XTmrCtr_GetValue(&xps_timer_0, XPAR_XPS_TIMER_0_DEVICE_ID);

	bubblesort = teste_sort_c(vet_c);

	EndTime = XTmrCtr_GetValue(&xps_timer_0, XPAR_XPS_TIMER_0_DEVICE_ID);

	for(i=0; i<SIZE; i++){
		xil_printf("vet_c[%d] = %d\r\n", i, vet_c[i]);
	}

	//xil_printf("Inicio: %d\r\n", BeginTime); //clock no inicio
	//xil_printf("Fim: %d\r\n", EndTime);		//clock no final

	TimeRun = (EndTime - BeginTime - Calibration);
	xil_printf("Tempo para exec. no uC: %d us\r\n\n", TimeRun/50); //base de tempo 1 clock = 20ns
	return 0;
}


