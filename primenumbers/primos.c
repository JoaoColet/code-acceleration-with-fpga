#include <stdio.h>
#include "xparameters.h"
#include "core_primos.h"
#include "xtmrctr.h"

int teste_primo_vhdl(int x){
	CORE_PRIMOS_mWriteReg(XPAR_CORE_PRIMOS_0_BASEADDR, CORE_PRIMOS_SLV_REG0_OFFSET, x);
	return CORE_PRIMOS_mReadReg(XPAR_CORE_PRIMOS_0_BASEADDR, CORE_PRIMOS_SLV_REG1_OFFSET);
}

int teste_primo_c(int x){
	int i;
	for(i=2; i<=x/2; i++){
		if(x%i==0){
			return 0;
		}
	}
	return 1;
}

int main(){
	XTmrCtr xps_timer_0;
	XTmrCtr* timer_0 = &xps_timer_0;
	Xuint32 BeginTime;
	Xuint32 EndTime;
	Xuint32 Calibration;
	Xuint32 TimeRun;
	int status;
	status = XTmrCtr_Initialize(&xps_timer_0, XPAR_XPS_TIMER_0_DEVICE_ID);
	//xil_printf("status do timer (0 iniciou certo): %d\r\n", status);
	XTmrCtr_Start(&xps_timer_0, XPAR_XPS_TIMER_0_DEVICE_ID);

	BeginTime = XTmrCtr_GetValue(timer_0, XPAR_XPS_TIMER_0_DEVICE_ID);
	EndTime = XTmrCtr_GetValue(timer_0, XPAR_XPS_TIMER_0_DEVICE_ID);
	Calibration = EndTime - BeginTime;
	//xil_printf("Calibracao das chamadas de funcao timer: %d\r\n\n", Calibration);

	int a, b;
	int vet[22] = {2, 4, 37813, 2741, 4409, 6133, 7919, 9733, 11657, 13499, 15401, 17389,
				   19423, 21383, 23321, 25391, 27449, 29443, 31601, 33613, 35759, 37813};

	int i;
	for(i=0; i<3; i++){
		a=vet[i];
		BeginTime = XTmrCtr_GetValue(&xps_timer_0, XPAR_XPS_TIMER_0_DEVICE_ID);

		b = teste_primo_vhdl(a);

		EndTime = XTmrCtr_GetValue(&xps_timer_0, XPAR_XPS_TIMER_0_DEVICE_ID);

		if(b==1){
			xil_printf("%d -> %d\r\n", a, b);
		}
		if(b==0){
			xil_printf("%d -> %d\r\n", a, b);
		}

		TimeRun = (EndTime - BeginTime - Calibration);
		xil_printf("Tempo para exec. na FPGA: %d us\r\n", TimeRun/50); //base de tempo 1 clock = 20ns, ou seja 50 clks = 1 us

		BeginTime = XTmrCtr_GetValue(&xps_timer_0, XPAR_XPS_TIMER_0_DEVICE_ID);

		b = teste_primo_c(a);

		EndTime = XTmrCtr_GetValue(&xps_timer_0, XPAR_XPS_TIMER_0_DEVICE_ID);

		if(b==1){
			xil_printf("%d -> %d\r\n", a, b);
		}
		if(b==0){
			xil_printf("%d -> %d\r\n", a, b);
		}

		TimeRun = (EndTime - BeginTime - Calibration);
		xil_printf("Tempo para exec. no uC: %d us\r\n\n", TimeRun/50); //base de tempo 1 clock = 20ns
	}
	return 0;
}


